const fs = require('fs');

const createdDate = (file) => {  
  const { birthtime } = fs.statSync(file);

  return birthtime;
}

const exists = (file) => {
  return fs.existsSync(file)
}

module.exports = {
    createdDate,
    exists
}
