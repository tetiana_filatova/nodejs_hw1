const express = require('express');
const path = require('path');
const morgan = require('morgan');
const fs = require('fs').promises;

const { createdDate, exists } = require('./funcs');

const PORT = 8080;
const dir = './files/';

const app = express();

app.use(express.json());
app.use(morgan(':date[clf] :method :url :status :res[content-length] - :response-time ms'));

app.get('/api/files', async (req, res) => {
    try {
        const files = await fs.readdir(dir);
        const message = 'Success';
        res.json({
            message,
            files})
    } catch(err) {
        console.log(`Error: ${err}`);
        const message = 'Client error';
        res.status(400).json({message});
    }
})

app.post('/api/files', (req, res) => {
    const filename = req.body.filename;
    let content = req.body.content;
    const extensions = ['.json', '.log', '.txt', '.jaml', '.xml', '.js'];
    const extension = filename ? path.extname(filename) : '';
    let validExt = false;
    if(extensions.includes(extension)){
        validExt = true;
    }
    if(!filename || !content || !validExt){
        const message = "Please specify 'content' parameter";
        res.status(400).json({message});
    } else {
        if(extension === '.json'){
            content = JSON.stringify(content);
        }
        (async () => {
            try {
                if(!exists(dir + filename)){
                    if (!exists(dir)){
                        await fs.mkdir(dir);
                    }
                    await fs.writeFile(dir + filename, content, 'utf8');
                    const message = 'File created successfully';
                    res.json({message});
                } else {
                    const message = "File already exists";
                    res.status(400).json({message});
                }
            } catch(err) {
                console.log(`Error: ${err}`);
            }
        })();
    }
})

app.get('/api/files/:filename', async (req, res) => {
    const filename = path.basename(req.url);
    const [,extension] = path.extname(filename).split('.');
    try {
        let content = await fs.readFile(dir + filename, 'utf8');
        if (path.extname(filename) === '.json'){
            content = JSON.parse(content);
        }
        const message = 'Success';
        const uploadedDate = createdDate(dir + filename);
        res.json({
            message,
            filename,
            content,
            extension,
            uploadedDate
        });
    } catch(err) {
        console.log(`Error: ${err}`);
        const message = `No file with '${filename}' filename found`;
        res.status(400).json({message});
    }
})

app.patch('/api/files/:filename', async (req, res, next) => {
    const filename = path.basename(req.url);
    const newContent = req.body.content;
    try {
        if(exists(dir) && exists(dir + filename)){
            await fs.writeFile(dir + filename, newContent, 'utf8');
            const message = 'File updated successfully';
            res.json({message});
        } else {
            const message = `No file with '${filename}' filename found`;
            res.status(400).json({message});
        }
    } catch(err) {
        console.log(`Error: ${err}`);
        const message = `No file with '${filename}' filename found`;
        res.status(400).json({message});
    }
})

app.delete('/api/files/:filename', async (req, res) => {
    const filename = path.basename(req.url);
    try {
        await fs.unlink(dir + filename);
        const message = `'${filename}' was deleted`;
        res.json({message});
    } catch(err) {
        console.log(`Error: ${err}`);
        const message = `No file with '${filename}' filename found`;
        res.status(400).json({message});
    }
})

app.use((req, res, next) => {
    const message = 'Page not found';
    res.status(404).json({message});
})

app.use((error, req, res, next) => {
    const message = error.status ? error.message : 'Server error';
    res.status(error.status || 500).json({message});
})

app.listen(PORT);
console.log(`Server listening on port ${PORT}`);